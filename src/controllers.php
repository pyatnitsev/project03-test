<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

//Request::setTrustedProxies(array('127.0.0.1'));

$app->get('/', function () use ($app) {

    $sql = "select * from event where event_at >= '2017-01-10 00:00:00' and event_at <= '2017-01-10 23:59:59' order by user_id, event_at";
    $times =  $app['db']->fetchAll($sql);
    $eventType = null;
    $firstTime = null;
    $secondTime = null;

    $timetable = [];
    foreach ($times as $time) {
        $firstTime = \Carbon\Carbon::parse($time['event_at']);
        if ($eventType != $time['type']) {
            $oldType = $eventType;
            $eventType = $time['type'];
            $timetable[$time['user_id']][$oldType][] = $firstTime->diffInSeconds($secondTime);
            $secondTime = \Carbon\Carbon::parse($time['event_at']);
        }
    }
    $users = $app['db']->fetchAll('select * from user where id in (?)', [implode(',',array_keys($timetable))]);



    return $app['twig']->render('index.html.twig', array('timetable' => $timetable, 'users' => $users));
})
->bind('homepage')
;

$app->error(function (\Exception $e, Request $request, $code) use ($app) {
    if ($app['debug']) {
        return;
    }

    // 404.html, or 40x.html, or 4xx.html, or error.html
    $templates = array(
        'errors/'.$code.'.html.twig',
        'errors/'.substr($code, 0, 2).'x.html.twig',
        'errors/'.substr($code, 0, 1).'xx.html.twig',
        'errors/default.html.twig',
    );

    return new Response($app['twig']->resolveTemplate($templates)->render(array('code' => $code)), $code);
});
